FROM golang:1.15-alpine AS builder
WORKDIR /usr/src/app
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -v -a -o ecs-deploy .

FROM alpine
RUN apk --no-cache add ca-certificates
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
WORKDIR /usr/local/bin
COPY --from=builder /usr/src/app/ecs-deploy .
CMD ["./ecs-deploy"]
