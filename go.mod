module gitlab.com/jzafran/ecs-deploy

go 1.15

require (
	github.com/aws/aws-sdk-go v1.36.2
	github.com/urfave/cli/v2 v2.3.0
)
