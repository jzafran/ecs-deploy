package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ecs"
	"log"
	"os"
)
import "github.com/urfave/cli/v2"

func ecsDeploy(sess *session.Session, cluster, service, image string) error {
	client := ecs.New(sess)

	var ecsService *ecs.Service
	if output, err := client.DescribeServices(&ecs.DescribeServicesInput{
		Cluster:  aws.String(cluster),
		Services: aws.StringSlice([]string{service}),
	}); err != nil {
		return err
	} else if len(output.Services) != 1 {
		return fmt.Errorf("%d services found for %q in cluster %q", len(output.Services), service, cluster)
	} else {
		ecsService = output.Services[0]
	}

	var taskDefinition *ecs.TaskDefinition
	var taskDefinitionTags []*ecs.Tag
	if taskDefOutput, err := client.DescribeTaskDefinition(&ecs.DescribeTaskDefinitionInput{
		TaskDefinition: ecsService.TaskDefinition,
		Include:        aws.StringSlice([]string{"TAGS"}),
	}); err != nil {
		return err
	} else {
		taskDefinition = taskDefOutput.TaskDefinition
		taskDefinitionTags = taskDefOutput.Tags
	}
	if len(taskDefinition.ContainerDefinitions) != 1 {
		return fmt.Errorf("only 1 container definition supported; found %v", len(taskDefinition.ContainerDefinitions))
	}
	taskDefinition.ContainerDefinitions[0].SetImage(image)

	log.Printf("Updating task definition %q", aws.StringValue(taskDefinition.TaskDefinitionArn))
	var taskDefinitionArn string
	taskDefInput := &ecs.RegisterTaskDefinitionInput{
		ContainerDefinitions:    taskDefinition.ContainerDefinitions,
		Cpu:                     taskDefinition.Cpu,
		ExecutionRoleArn:        taskDefinition.ExecutionRoleArn,
		Family:                  taskDefinition.Family,
		IpcMode:                 taskDefinition.IpcMode,
		Memory:                  taskDefinition.Memory,
		NetworkMode:             taskDefinition.NetworkMode,
		PidMode:                 taskDefinition.PidMode,
		ProxyConfiguration:      taskDefinition.ProxyConfiguration,
		TaskRoleArn:             taskDefinition.TaskRoleArn,
	}
	if len(taskDefinition.InferenceAccelerators) > 0 {
		taskDefInput.InferenceAccelerators = taskDefinition.InferenceAccelerators
	}
	if len(taskDefinition.PlacementConstraints) > 0 {
		taskDefInput.PlacementConstraints = taskDefinition.PlacementConstraints
	}
	if len(taskDefinition.RequiresCompatibilities) > 0 {
		taskDefInput.RequiresCompatibilities = taskDefinition.RequiresCompatibilities
	}
	if len(taskDefinition.Volumes) > 0 {
		taskDefInput.Volumes = taskDefinition.Volumes
	}
	if len(taskDefinitionTags) > 0 {
		taskDefInput.Tags = taskDefinitionTags
	}

	if output, err := client.RegisterTaskDefinition(taskDefInput); err != nil {
		return err
	} else {
		taskDefinitionArn = aws.StringValue(output.TaskDefinition.TaskDefinitionArn)
	}

	log.Printf("Updating service %q", aws.StringValue(ecsService.ServiceName))
	if _, err := client.UpdateService(&ecs.UpdateServiceInput{
		Cluster:            ecsService.ClusterArn,
		ForceNewDeployment: aws.Bool(true),
		Service:            ecsService.ServiceName,
		TaskDefinition:     aws.String(taskDefinitionArn),
	}); err != nil {
		return err
	}

	log.Printf("Waiting until service %q is stable", aws.StringValue(ecsService.ServiceName))
	if err := client.WaitUntilServicesStable(&ecs.DescribeServicesInput{
		Cluster:  ecsService.ClusterArn,
		Services: []*string{ecsService.ServiceName},
	}); err != nil {
		return err
	}

	log.Printf("Done deplying ECS service")
	return nil
}

func main() {
	app := &cli.App{
		//Name:                   "",
		//Usage:                  "",
		//Version:                "",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "cluster",
				EnvVars:  []string{"ECS_CLUSTER"},
				Required: true,
			},
			&cli.StringFlag{
				Name:     "service",
				EnvVars:  []string{"ECS_SERVICE"},
				Required: true,
			},
			&cli.StringFlag{
				Name:     "image",
				EnvVars:  []string{"ECS_IMAGE"},
				Required: true,
			},
			&cli.StringFlag{
				Name:     "region",
				EnvVars:  []string{"ECS_REGION", "AWS_REGION", "AWS_DEFAULT_REGION"},
				Required: true,
			},
		},
		Action: func(c *cli.Context) error {
			cluster := c.String("cluster")
			service := c.String("service")
			image := c.String("image")
			region := c.String("region")

			sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(region)}))
			return ecsDeploy(sess, cluster, service, image)
		},
		//Compiled:               time.Time{},
		//Authors:                nil,
		//Copyright:              "",
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
